//Developing production system

#include <iostream>
#include <stdlib.h>
#include <QString>
#include <QVector>
#include <QFile>
#include <QList>
#include <QTextStream>
#include <QDebug>

using namespace std;

class prod
{
private:
    struct sent                 //структура объекта списка
    {
        QString fact_from_1;
        QString fact_from_2;
        QString fact_from_3;
        QString fact_to;
        QString link_1;
        QString link_2;
        QString link_3;
        sent(QString fact_f, QString fact_t, QString l)
        {
           this->fact_from_1 = fact_f;
           this->fact_to = fact_t;
           this->link_1 = l;
        }
        sent (QString fact_f_1, QString l_1, QString fact_f_2, QString l_2, QString fact_t)
        {
            this->fact_from_1 = fact_f_1;
            this->link_1 = l_1;
            this->fact_from_2= fact_f_2;
            this->link_2 = l_2;
            this->fact_to = fact_t;
        }
        sent (QString fact_f_1, QString l_1, QString fact_f_2, QString l_2, QString fact_f_3, QString l_3, QString fact_t)
        {
            this->fact_from_1 = fact_f_1;
            this->link_1 = l_1;
            this->fact_from_2= fact_f_2;
            this->link_2 = l_2;
            this->fact_from_3 = fact_f_3;
            this->link_3 = l_3;
            this->fact_to = fact_t;
        }
    };
    QVector <QString> sentences;
    QList <sent*> elements;
    //QString temp;
    //QStringList list;
public:
    void read_file (const char* filename);
    void show_content ();
    void split_sent_struct ();
    bool chain();
    bool work_3 (int i, int j);
    bool work_5 (int i, int j, int p);
    bool work_7 (int i, int j, int p);
};

void prod::read_file(const char* filename)
{
    QFile file (filename);
    file.open(QIODevice::ReadOnly);
    if(file.size()==0)
    {
        cerr << "error opening " << endl;
        exit(1);
    }
    QTextStream in (&file);
    while(!in.atEnd())
    {
    QString tmp=in.readLine();
    sentences.push_back(tmp);
    }

    file.close();
}

void prod::show_content()
{
    QVector<QString>::const_iterator cont_it;
    cout<<"Sentences: "<<endl;
        for (cont_it = sentences.begin(); cont_it!=sentences.end(); ++cont_it)
        {
            qDebug() << *cont_it;
        }
}

void prod::split_sent_struct()
{
 //   cout<<"Now spliting"<<endl;
    QStringList list;
    QString tmp;
    QVector<QString>::const_iterator cont_it;
    for (cont_it = sentences.begin(); cont_it!=sentences.end(); ++cont_it)
    {
        tmp = *cont_it;
        list = tmp.split(" ");
        //создаем очередной элемент списка типа структуры sent
        //в зависимости от количества слов в строке вызываем определенный конструктор
        int kol=list.size();
        if (kol==3)
        {
            sent *S = new sent (list[0], list [2], list [1]);
            elements.push_back(S);
        }
        else
        {
            if (kol==5)
            {
                sent *S = new sent (list[0], list[1], list[2], list[3], list[4]);
                elements.push_back(S);
            }
            else
            {
                if (kol==7)
                {
                    sent *S = new sent (list[0], list[1], list[2], list[3], list[4], list[5], list[6]);
                    elements.push_back(S);
                }
            }
        }
    }

    //QList <sent*>::iterator iter=element.begin(); iter<element.end(); ++iter
}

bool prod::work_3 (int i, int j)
{
    QString temp=" ";
    QStringList list;
    bool flag = false;      //флаг изменений
    bool the_end_of_the_base = false;
    if (elements[i]->fact_to==elements[j]->fact_from_1)         //если найдено совпадение
     {
      //создаем новое утверждение
      temp=elements[i]->fact_from_1+" "+elements[i]->link_1+" "+elements[j]->fact_to;
     }
    //если наша строка не пустая
    if (temp!=" ")
       {
         //cout<<"not empty"<<endl;
         //то превращаем новое утверждение в элемент списка
         list=temp.split(" ");
         sent *S=new sent (list[0], list[2], list[1]);
         //пихать в сам список пока нет смысла, вдруг он уже там имеется
         //проверка на наличие в базе найденного предложения
         bool got_in_base=false;
         for (int a=0; a<elements.count(); ++a)
         {
          if ((elements[a]->fact_from_1==S->fact_from_1)&&(elements[a]->fact_to==S->fact_to)&&(elements[a]->link_1==S->link_1))
            {
             got_in_base=true;
             //qDebug()<<"we already got it: "<<temp;
             delete S;
             if ((i==elements.size()-1)&&(j==elements.size()-2))the_end_of_the_base=true;      //если конец базы, то пора выходить, добавлять нечего, конец
              else break;          //если не конец, продолжаем идти по базе
            }
         }
         if (got_in_base==false)     //если совпадений не найдено, добавляем в базу
         {
            elements.push_back(S);
            //а теперь придется перезапускать ф-ю, т.к. количество элементов изменилось
            flag=true;     //изменение в базе произошло, что говорит о необх-ти перезапуска
            //qDebug()<<"Added: "<<temp;
            cout<<endl;
         }
       }
    //если нет совпадений у сравниваемых предложений и наша строка пустая
    else
    {
        // проверяем, не конец ли это базы
        //ведь если конец, то пора выходить
        if ((i==elements.size()-1)&&(j==elements.size()-2)){ the_end_of_the_base=true;}      //если конец базы, то пора выходить, добавлять нечего, конец
    }
    int k=elements.size()-1;
    temp=elements[k]->fact_from_1+" "+elements[k]->link_1+" "+elements[k]->fact_from_2+" "+elements[k]->link_2+" "+elements[k]->fact_to;
    if (the_end_of_the_base==true){qDebug()<<"quod erat demonstrandum: "<<temp;}
    return flag;
}


bool prod::work_5 (int i, int j, int p)
{
    QString temp=" ";
    QStringList list;
    bool flag = false;
    bool the_end_of_the_base = false;
    if (elements[i]->fact_from_1==elements[j]->fact_from_1)
    {
        temp=elements[j]->fact_to+" "+elements[i]->link_1+" "+elements[i]->fact_from_2+" "+elements[i]->link_2+" "+elements[i]->fact_to;
    }
    if (elements[i]->fact_from_2==elements[j]->fact_from_1)
    {
        temp=elements[i]->fact_from_1+" "+elements[i]->link_1+" "+elements[j]->fact_to+" "+elements[i]->link_2+" "+elements[i]->fact_to;
    }
    if (elements[i]->fact_to==elements[j]->fact_from_1)
    {
        temp=elements[i]->fact_from_1+" "+elements[i]->link_1+" "+elements[i]->fact_from_2+" "+elements[i]->link_2+" "+elements[j]->fact_to;
    }
    //qDebug()<<temp<<endl;
    //если наша строка не пустая
    if (temp!=" ")
     {
        //то превращаем новое утверждение в элемент списка
        list=temp.split(" ");
        sent *S=new sent (list[0], list[1], list[2], list[3], list[4]);
        //проверяем, есть ли такое в базе
        bool got_in_base=false;
        for (int a=0; a<elements.count(); ++a)
        {
         if ((elements[a]->fact_from_1==S->fact_from_1)&&(elements[a]->link_1==S->link_1)&&(elements[a]->fact_from_2==S->fact_from_2)&&(elements[a]->link_2==S->link_2)&&(elements[a]->fact_to==S->fact_to))
          {
            got_in_base=true;
            // qDebug()<<"we already got it: "<<temp;
            delete S;
            if ((i==elements.size()-1)&&(j==elements.size()-2))the_end_of_the_base=true;      //если конец базы, то больше делать нечего
            else break;          //если мы не дошли до конца базы, то продолжаем прохождение
          }
        }
        if (got_in_base==false)     //если совпадений не найдено, добавляем в базу
        {
         elements.push_back(S);
         //qDebug()<<"Added: "<<temp;
         //а теперь придется перезапускать ф-ю, т.к. количество элементов изменилось
         flag=true;     //изменение в базе произошло, что говорит о необх-ти перезапуска
        }
     }
    //если нет совпадений у сравниваемых предложений и наша строка пустая
    else
    {
        // проверяем, не конец ли это базы
        //ведь если конец, то пора выходить
        if ((i==elements.count()-1)&&(j==p)){the_end_of_the_base=true;};      //если конец базы, то пора выходить, добавлять нечего, конец
    }
    int k=elements.size()-1;
    temp=elements[k]->fact_from_1+" "+elements[k]->link_1+" "+elements[k]->fact_from_2+" "+elements[k]->link_2+" "+elements[k]->fact_from_3+" "+elements[k]->link_3+" "+elements[k]->fact_to;
    if (the_end_of_the_base==true){qDebug()<<"quod erat demonstrandum: "<<temp;}
    return flag;
}

bool prod::work_7 (int i, int j, int p)
{
    QString temp=" ";
    QStringList list;
    bool flag = false;
    bool the_end_of_the_base = false;
    if (elements[i]->fact_from_1==elements[j]->fact_from_1)
    {
        temp=elements[j]->fact_to+" "+elements[i]->link_1+" "+elements[i]->fact_from_2+" "+elements[i]->link_2+" "+elements[i]->fact_from_3+" "+elements[i]->link_3+" "+elements[i]->fact_to;
    }
    if (elements[i]->fact_from_2==elements[j]->fact_from_1)
    {
        temp=elements[i]->fact_from_1+" "+elements[i]->link_1+" "+elements[j]->fact_to+" "+elements[i]->link_2+" "+elements[i]->fact_from_3+" "+elements[i]->link_3+" "+elements[i]->fact_to;
    }
    if (elements[i]->fact_from_3==elements[j]->fact_from_1)
    {
        temp=elements[i]->fact_from_1+" "+elements[i]->link_1+" "+elements[i]->fact_from_2+" "+elements[i]->link_2+" "+elements[j]->fact_to+" "+elements[i]->link_3+" "+elements[i]->fact_to;
    }
    if (elements[i]->fact_to==elements[j]->fact_from_1)
    {
        temp=elements[i]->fact_from_1+" "+elements[i]->link_1+" "+elements[i]->fact_from_2+" "+elements[i]->link_2+" "+elements[i]->fact_from_3+" "+elements[i]->link_3+" "+elements[j]->fact_to;
    }
    if (temp!=" ")
        {
            list=temp.split(" ");
            sent *S=new sent (list[0], list[1], list[2], list[3], list[4], list[5], list[6]);
            //проврряем, есть ли такое в базе
            bool got_in_base=false;
            for (int a=0; a<elements.size(); ++a)
            {
             if ((elements[a]->fact_from_1==S->fact_from_1)&&(elements[a]->fact_from_2==S->fact_from_2)&&(elements[a]->fact_from_3==S->fact_from_3)&&(elements[a]->fact_to==S->fact_to))
              {
               got_in_base=true;
               // qDebug()<<"we already got it: "<<temp;
               delete S;
               if ((i==elements.size()-1)&&(j==p)) the_end_of_the_base = true;      //если конец базы, то больше делать нечего
               else break;          //если мы не дошли до конца базы, то продолжаем прохождение
              }
            }
            if (got_in_base==false)     //если совпадений не найдено, добавляем в базу
            {
             elements.push_back(S);
             //qDebug()<<"Added this one: "<<temp;
             //а теперь придется перезапускать ф-ю, т.к. количество элементов изменилось
             flag=true;     //изменение в базе произошло, что говорит о необх-ти перезапуска
            }
        }
    else
    {
        // проверяем, не конец ли это базы
        //ведь если конец, то пора выходить
        if ((i==elements.size()-1)&&(j==p)){ the_end_of_the_base=true;}      //если конец базы, то пора выходить, добавлять нечего, конец
    }
    int k=elements.size()-1;
    temp=elements[k]->fact_from_1+" "+elements[k]->link_1+" "+elements[k]->fact_from_2+" "+elements[k]->link_2+" "+elements[k]->fact_from_3+" "+elements[k]->link_3+" "+elements[k]->fact_to;
    if (the_end_of_the_base==true){qDebug()<<"quod erat demonstrandum: "<<temp;}
    return flag;
}

bool prod::chain()
{
    // поиск последнего предложения из 3 элементов
    int p;
    for (int l=elements.size()-1; l>=0; l--)
    {
        if ((elements[l]->fact_from_2.isEmpty())&&(elements[l]->fact_from_3.isEmpty())) p=l;
    }
    //изменений в базе не произошло, значит
    bool develop=false;
    //осуществляем поиск новых суждений, сравнивая fact_from и fact_to у разных предложений
    for (int i=0; i<elements.size(); ++i)
    {
     for (int j=0; j<elements.size(); ++j)
     {
      //если i-ое сообщение состоит из 3 элементов и сравнивается c подобным себе
      if ((i!=j)&&(elements[i]->fact_from_2.isEmpty())&&(elements[j]->fact_from_2.isEmpty()))
      {
       //cout<<"It's 3"<<endl;
       develop = work_3(i, j);
      }
      else //если i-ое сообщение состоит из 5 и сравнивается с 3
      {
       if ((i!=j)&&(elements[i]->fact_from_3.isEmpty())&&(elements[j]->fact_from_2.isEmpty()))
        {
         //cout<<"It's 5"<<endl;
         develop = work_5(i, j, p);
        }
       else
        {
         if (!(elements[i]->fact_from_3.isEmpty())&&(elements[j]->fact_from_2.isEmpty()))
           {
             //cout<<"It's 7"<<endl;
             develop = work_7(i ,j, p);
           }
        }
      }
      if (develop==true) break;
     }
     if (develop==true) break;
    }
    return develop;
}


int main ()
{
    cout<<"Ka boom!"<<endl;
    prod A;
    cout<<"What theorem you want to prove?"<<endl;
    cout<<"the Pythagorean theorem - 1"<<endl;
    cout<<"the theorem on the sum of the angles of a triangle - 2"<<endl;
    cout<<"your choice: ";
    int b;
    cin>>b;
    if (b==1)
        {
            cout<<"the Pythagorean theorem"<<endl;
            A.read_file("1.txt");
        }
      else
        {
            cout<<"the theorem on the sum of the angles of a triangle"<<endl;
            A.read_file("2.txt");
        }

    A.show_content();

    A.split_sent_struct();

    //начинаем проходить нашу базу с самого начала
    bool develop;
    cout<<"ACTION!"<<endl;
    do
    {
    // cout<<"DO!"<<endl;
     develop=A.chain ();
    } while (develop!=false);  //до тех пор, пока менять уже будет нечего
    system ("pause");
    return 0;
}
